var board = document.getElementsByTagName("td");

           // assuming we index the 9 tic tac toe cells from left to right, top to

var winSets = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

         // the 'X' always gets to go first
var player = "X";

//to know how many cells are empty at any time
var empty = 9;

// keep track of game status if still playing
var gameOver = false;


function cellClicked(cell) {

  
if ((board[cell].innerHTML ===0) && (!gameOver))
        {
         empty -= 1;        // decrease # of empty cells by 1
        checkWin();
        player = (player === "X") ? "O" : "X";
        document.getElementById("player").innerHTML = player;
       }
}


function checkWin() {

       for ( i = 0; i < winSets.length; i++) {
      if (board[winSets[i][0]].innerHTML === board[winSets[i][1]].innerHTML
       && board[winSets[i][1]].innerHTML === board[winSets[i][2]].innerHTML
       && board[winSets[i][0]].innerHTML !== "") {
  
           console.log("We have a winner!");
   gameOver = true;
         //display the winner
 document.getElementById("winner").innerHTML = player+" Wins";

    displayWin(true);

break;
}
}


if (!empty)
{
// to show the game is now over
gameOver = true;

document.getElementById("winner").innerHTML = "No one wins";
displayWin(true);
}
  
}
function resetGame() {

for ( i = 0; i < board.length; i++) {
board[i].innerHTML = "";
}
  //resetting player back to X
  player = "X";
document.getElementById("player").innerHTML = player;
  
gameOver = false;
empty = 9;
}

document.getElementById("reset").addEventListener("click", resetGame);
document.getElementById("message").addEventListener("click", function() {
displayWin(false);
});
for ( i = 0; i < board.length; i++) {
document.getElementsByTagName("td")[i].addEventListener("click", function() {
cellClicked(this);
});
}

